.. _usage_guide:

***********
Usage Guide
***********

Introduction
============
| Aurora is a set of interconneted nodes living on multiple machines
| To set it up you will need to copy this folder onto each machine, configure and run it

Creating a virtual environment
==============================
| You need to create a virtual environment with python >= 3.7
| Run the command
::
    python -m venv virenv
    source virenv/bin/activate

Installing required packages
============================
| To run Aurora you will need the packages listed in the `requirements.txt` file.
| To install the packages type: 
::
    
    pip3 install -r requirements.txt
    
Creating new node
=================
| To create new node create new subfolder ex myNode
| And copy config.yaml and certs folder there

Configuring a local node
======================
| You must configure the node to give it the addresses of other nodes as well as information on what commands it can run.
| Node config can be found in config.yaml
| The options include:
* name
    The name that will be used to refer to this node. You have to modify it because having multiple nodes with the same name in the network might cause problems.
* port
    Port on which the node will be available

Generating node certificate
===========================
| Now you need to generate SSL certificate for this node
| To do this enter certs directory then run
|
::

    ./generate_cert.sh myNode 10.0.1.23

| Where myNode is the chosen node name
| And address is the ip address that the node will be visible on

Running the node
================
To run the node run the command:
::
    python3 -m node

Managing your node
==================
| You can manage your lcoal node with a web or command line interface
| In each case you will need local access password
| You can find this password in one of the first lines of node output
| Local password is: <local password> 
* Command line interface
    In your node's directory run `python -m node_cli` and login with local password
    Type `help` for more information
* Web interface
    To start web interface you need npm
    in dircetory /web run ``npm start``, on first time you will need ``npm install``
    Now you can access web interface on https://localhost:3000
    Insert address of local node and login with local user
    Only accessing local nodes is safe

Configuring secure connection between nodes
===========================================
| Each pair of nodes that wants to exchange information needs first
| to setup a trusted connection
| Let's say nodeA on 127.0.0.1:8080 and nodeB on 127.0.0.1:8081 want to connect
* Command line interface
    login to command line interface of nodeA and nodeB
    on nodeA do `conn -c nodeB 127.0.0.1:8081`
    Now nodes have exchanged information
    Run conn -p on nodeA and nodeB
    Verify that hashes are the same on both nodes
    If hashes are the same on both nodes run conn -a <hash>

* Web interface
    Login to web interface on both nodes
    Go to 'Authorization' tab
    Right click onto left rectangle -> Init Handshake and enter nodeB, 127.0.0.1:8081
    Now nodes have exchanged information and connection is waiting for user approval
    Verify that hashes on both nodes are the same
    Then on both nodes right click on pending connection and accept it

Groups
======
| Nodes can be organized into groups
| then commands can be send to whole group or one node in a given group
| Interfaces contain intuitive ways to manage them

Adding tasks on nodes
=====================
| To create a task create folder tasks in your node directory
| Then create file __init__.py
| Example __init__.py adding user task defined by /tasks/lights:
::

    from node.nexus import Nexus
    from flask import request as req
    import webbrowser as wb

    class Local:
        def __init__(self, nx: Nexus):
            @nx.app.route('/tasks/lights', methods=['POST'])
            @nx.httpauth.login_required
            def lights():
                print(f'New lights\' state: {req.json.get("state") or "N/A"}')
                return ''

Adding voice commands
=====================
| To add a voice command first enable voice in web/cl interface
| Then ???

Running command without voice
=============================
| You can run commands without voice from command line
| ``do lights`` will run command mapped to /tasks/lights